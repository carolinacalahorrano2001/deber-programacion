#Escuela Politécnica Nacional
#Programación
#Auntor: Carolina Calahorrano
#ejemplo 11: pandas- análisis de datos 

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
#cuando se usa pandas no se necesita x y y,
#se plotea las filas en función de las columnas 

#==========================================================
#df = pd.DataFrame(np.random.randn(100, 5), columns=list('ABCDE'))
#df=df.cumsum() # Return cumulative sum over a DataFrame or Series axis

#df.plot() #plot de curvas
#df.plot.bar(stacked=True) #plot de barras
#df.plot.bar(stacked=False)#lineas
#df.plot.bar() #lineas

#plt.show()
#=========================================================
#se utilizan las mismas funciones que para plotear una curva
#df.plot.box() # caja de gato
#plt.show()
#==========================================================
#se desactivan todas las funciones anterioes 
data = pd.Series(np.random.normal(size=100)) #datos 
data.hist(grid=False)#Histograma
plt.show()
